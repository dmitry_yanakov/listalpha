

const timeLineTypes = {
    ex: 'exTimeLine',
    current: 'timeLine',
}

/**
 * Prepare fetched data and display it.
 *
 * @returns {Promise<void>}
 */
const displayFetchedData = async (firstRun = false) => {
    const response = await fetch('/fetch-timeline-data')   //router.get('/fetch-timeline-data', indexController.getDataForTimeLine)
    const data = await response.json()

     

    //if (firstRun) {
    //   console.log("First run");
        //displayEmployeesNames(data.listOfEmployees) old function
        //const preparedData = prepareDataForTimeLine(employees)
        //displayGoogleTimeLine_1(data, "company1", "Ex-employees")

    //}


    data.listOfEmployees.map(employees => {
        
        //error check for bad files
       if (employees[0] == null) {
          console.log("bad row, need to skip this")
            return
        }

        const timeLineType = employees[0].dateRange.includes('Present') ? timeLineTypes.current : timeLineTypes.ex
        const preparedData = prepareDataForTimeLine(employees)
        displayGoogleTimeLine(preparedData, employees[0].companyName, timeLineType)
    })
    enableButtons()
}



const displayList = listOfEmployees => {
    listOfEmployees.map(typeOfEmployee => {



        const employeesForType = typeOfEmployee.map(employee => employee.name)
        const containerClass = typeOfEmployee[0].dateRange.includes('Present') ? '.current-employees-list' : '.ex-employees-list'
        appendEmployeesName(employeesForType, containerClass)
    })
}







/**
 * Prepare fetched data and display it.
 *
 * @returns {Promise<void>}
 */
const loadEmployeeList = async (firstRun = false) => {
   
    const fileName = 'list_1.json'
    const rawData = fs.readFileSync(path.join(__dirname, '../results_library/', fileName))
    const a = JSON.parse(rawData)
    
    console.log("file read!")

}





/**
 * Display employees names
 *
 * @param listOfEmployees
 */
const displayEmployeesNames = listOfEmployees => {
    listOfEmployees.map(typeOfEmployee => {

        //error check for bad files
        if (typeOfEmployee[0] == null) {
            console.log("bad file, need to skip this")
            return
        }

        const employeesForType = typeOfEmployee.map(employee => employee.name)
        const containerClass = typeOfEmployee[0].dateRange.includes('Present') ? '.current-employees-list' : '.ex-employees-list'
        appendEmployeesName(employeesForType, containerClass)
    })
}

/**
 * Append employees names to DOM.
 *
 * @param employees
 * @param containerClass
 */
const appendEmployeesName = (employees, containerClass) => {
    document.querySelector(containerClass).innerHTML = ""
    employees.map(employee => {
        const li = document.createElement('li')
        li.innerText = employee
        document.querySelector(containerClass).style.display = 'block'
        document.querySelector(containerClass).prepend(li)
    })
}

/**
 * Append loading line to DOM.
 */
const createLoadingLine = () => {
    if (document.getElementById('timeLineRefreshing') != null) {
        document.getElementById('timeLineRefreshing').remove()
    }

    const span = document.createElement('span')
    span.innerHTML = '<span id="timeLineRefreshing">' +
        '                 Timeline data is still refreshing...' +
        '             </span>'
    document.querySelector('.refresh-block').append(span)
}

/**
 * Prepare fetched data for displaying.
 *
 * @param data
 * @returns {[]}
 */
const prepareDataForTimeLine = data => {
    let resultedArray = []
    for (let i = 0; i < data.length; i++) {
        resultedArray.push([
            data[i].name,
            data[i].jobTitle,
            new Date(data[i].startYear, data[i].startMonth, 1),
            new Date(data[i].finishYear, data[i].finishMonth, 1)
        ])
    }
    return resultedArray
}

/**
 * Display TimeLine.
 *
 * @param resultedArray
 */
const displayGoogleTimeLine = (resultedArray, companyName, timeLineType) => {
    google.charts.load('current', {'packages': ['timeline']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        let elementType = {}

        if (timeLineType === 'exTimeLine') {
            elementType = {elementId: timeLineTypes.ex, description: 'Ex-employees'}
        } else {
            elementType = {elementId: timeLineTypes.current, description: 'Current employees'}
        }

        var container = document.getElementById(elementType.elementId);
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();

        dataTable.addColumn({type: 'string', id: 'Name'});
        dataTable.addColumn({type: 'string', id: 'Job title'});
        dataTable.addColumn({type: 'date', id: 'Start'});
        dataTable.addColumn({type: 'date', id: 'End'});
        dataTable.addRows(resultedArray)

        chart.draw(dataTable);
        prependCompanyNameTitle(container, `${companyName} - ${elementType.description}`)
        document.querySelector('.lds-dual-ring').style.display = 'none'
    }
}

/**
 * Display TimeLine.
 *
 * @param resultedArray
 */
const displayGoogleTimeLine_1 = (resultedArray, companyName, timeLineType) => {
    google.charts.load('current', {'packages': ['timeline']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        let elementType = {}

        if (timeLineType === 'exTimeLine') {
            elementType = {elementId: timeLineTypes.ex, description: 'Ex-employees'}
        } else {
            elementType = {elementId: timeLineTypes.current, description: 'Current employees'}
        }

        var container = document.getElementById(elementType.elementId);
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();

        dataTable.addColumn({type: 'string', id: 'Name'});
        dataTable.addColumn({type: 'string', id: 'Job title'});
        dataTable.addColumn({type: 'date', id: 'Start'});
        dataTable.addColumn({type: 'date', id: 'End'});
        dataTable.addRows(resultedArray)

        chart.draw(dataTable);
        prependCompanyNameTitle(container, `${companyName} - ${elementType.description}`)
        document.querySelector('.lds-dual-ring').style.display = 'none'
    }
}




/**
 * Prepend company name into DOM.
 *
 * @param companyName
 */
const prependCompanyNameTitle = (container, companyName) => {
    const companyNameNode = document.createElement('h2')
    companyNameNode.innerText = companyName
    companyNameNode.style.marginTop = '0'
    container.prepend(companyNameNode)
}

/**
 * Reload employees data and TimeLine table.
 *
 * @returns {Promise<void>}
 */
const refreshTimeLine = async e => {
    disableButtons()
    createLoadingLine()

    if (e !== undefined) {
        const employeesResponse = await fetch('/refresh-employees')
        const employeesData = await employeesResponse.json()
        const exEmployeesResponse = await fetch('/refresh-ex-employees')
        const exEmployeesData = await exEmployeesResponse.json()

        if (exEmployeesData.status === 'success') {
            checkEmployeesProgress(employeesData.data.containerId)
            checkExEmployeesProgress(exEmployeesData.data.containerId)
        }

    } else {
        const employeesResponse = await fetch('/refresh-employees')
        const employeesData = await employeesResponse.json()

        if (employeesData.status === 'success') {
            checkEmployeesProgress(employeesData.data.containerId)
        }
    }
}

/**
 * Check status of API progress.
 *
 * @param containerId
 * @returns {Promise<any>}
 */
const checkApiProgress = async containerId => {
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({containerId})
    }
    const apiProgressResponse = await fetch('/check-api-progress', options)
    const apiProgress = await apiProgressResponse.json()

    return apiProgress
}

/**
 * Check employees API progress and fetch next API.
 *
 * @param containerId
 */
const checkEmployeesProgress = containerId => {
    const checkInterval = setInterval(async () => {
        const apiProgress = await checkApiProgress(containerId)

        if (apiProgress.endType === "finished" && apiProgress.exitCode === 0) {
            clearInterval(checkInterval)

            const employeesNamesResponse = await fetch('/get-names', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({containerId})
            })
            const employeesNamesData = await employeesNamesResponse.json()
            let employeesNames = JSON.parse(employeesNamesData.resultObject)

            employeesNames[0].dateRange = 'настоящее время'
            employeesNames = employeesNames.filter(employee => employee.error === undefined)
            displayEmployeesNames([employeesNames])

            const employeesProgressResponse = await fetch('/refresh-employees-scraper')
            const employeesProgressData = await employeesProgressResponse.json()

            if (employeesProgressData.status === 'success') {
                checkEmployeesScraperProgress(employeesProgressData.data.containerId)
            }
        }
        if (apiProgress.exitCode === 1) {
            clearInterval(checkInterval)
            createErrorLine()
            enableButtons()
            document.getElementById('timeLineRefreshing').remove()
        }

    }, 10000)
}

/**
 * Enable main buttons on page.
 */
const enableButtons = () => {
    document.getElementById('refreshBtn').disabled = false
    document.getElementById('sendCompanyUrl').disabled = false
}

/**
 * Disable main buttons on page.
 */
const disableButtons = () => {
    document.getElementById('refreshBtn').disabled = true
    document.getElementById('sendCompanyUrl').disabled = true
}

/**
 * Append error line to DOM.
 */
const createErrorLine = (text = 'Incorrect LinkedIn url') => {
    if (document.getElementById('timeLineError') != null) {
        document.getElementById('timeLineError').remove()
    }

    const span = document.createElement('span')
    span.innerHTML = '<span id="timeLineError" style="color: red">' +
        '                 Incorrect LinkedIn url.' +
        '             </span>'
    document.querySelector('.refresh-block').append(span)
}

/**
 * Check employees scraper API progress and fetch next API.
 *
 * @param containerId
 */
const checkEmployeesScraperProgress = containerId => {
    const checkInterval = setInterval(async () => {

        const apiProgress = await checkApiProgress(containerId)

        if (apiProgress.endType === "finished") {
            const companyUrl = localStorage.getItem('companyUrl')

            clearInterval(checkInterval)
            const options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({containerId, companyUrl, exEmployees: false})
            }
            const rewriteResponse = await fetch('/rewrite-timeline-file', options)
            const rewriteData = await rewriteResponse.json()

            enableButtons()
            document.getElementById('timeLineRefreshing').remove()

            await displayFetchedData()
        }
    }, 15000)
}

/**
 *
 * @param containerId
 */
const checkExEmployeesScraperProgress = containerId => {
    const checkInterval = setInterval(async () => {

        const apiProgress = await checkApiProgress(containerId)

        if (apiProgress.endType === "finished") {
            const companyUrl = localStorage.getItem('companyUrl')

            clearInterval(checkInterval)
            const options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({containerId, companyUrl, exEmployees: true})
            }
            const rewriteResponse = await fetch('/rewrite-timeline-file', options)
            const rewriteData = await rewriteResponse.json()

            await displayFetchedData()
        }
    }, 15000)
}

/**
 * Check ex-employees API progress and fetch next API.
 *
 * @param containerId
 */
const checkExEmployeesProgress = containerId => {
    const checkInterval = setInterval(async () => {
        const apiProgress = await checkApiProgress(containerId)

        if (apiProgress.endType === "finished" && apiProgress.exitCode === 0) {
            clearInterval(checkInterval)

            const exEmployeesNamesResponse = await fetch('/get-names', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({containerId})
            })
            const exEmployeesNamesData = await exEmployeesNamesResponse.json()
            let exEmployeesNames = JSON.parse(exEmployeesNamesData.resultObject)

            exEmployeesNames[0].dateRange = 'ex-employee'
            exEmployeesNames = exEmployeesNames.filter(employee => employee.error === undefined)
            displayEmployeesNames([exEmployeesNames])

            const exEmployeesProgressResponse = await fetch('/refresh-ex-employees-scraper')
            const exEmployeesProgressData = await exEmployeesProgressResponse.json()

            if (exEmployeesProgressData.status === 'success') {
                checkExEmployeesScraperProgress(exEmployeesProgressData.data.containerId)
            }
        }
        if (apiProgress.exitCode === 1) {
            clearInterval(checkInterval)
            createErrorLine()
            enableButtons()
            document.getElementById('timeLineRefreshing').remove()
        }

    }, 10000)
}

/**
 * Check company info API progress and fetch next API
 *
 * @param containerId
 */
const checkCompanyInfoProgress = containerId => {
    const checkInterval = setInterval(async () => {
        const apiProgress = await checkApiProgress(containerId)

        if (apiProgress.endType === "finished" && apiProgress.exitCode === 0) {
            clearInterval(checkInterval)

            const options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({containerId})
            }
            const exEmployeesProgressResponse = await fetch('/launch-ex-employees', options)
            const employeesProgressData = await exEmployeesProgressResponse.json()

            if (employeesProgressData.status === 'success') {
                checkExEmployeesProgress(employeesProgressData.data.containerId)
            }
        }
        if (apiProgress.exitCode === 1) {
            clearInterval(checkInterval)
            createErrorLine()
            enableButtons()
            document.getElementById('timeLineRefreshing').remove()
        }

    }, 5000)
}

/**
 * Search TimeLine data by LinkedIn URL.
 *
 * @returns {Promise<void>}
 */
const searchByCompanyUrl = async () => {
    const companyUrl = document.getElementById('companyUrl').value
    if (companyUrl === '') {
        createErrorLine('Empty input')
        return
    }
    localStorage.setItem('companyUrl', companyUrl);
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({companyUrl})
    }
    const response = await fetch('/search-linkedin-url', options)
    const data = await response.json()

    if (data.status === 'success') {
        checkCompanyInfoProgress(data.data.containerId)
    }
    await refreshTimeLine()
}


//Ihar additions on Nov 30th 2020

const dodgyfunction = async (firstRun = false) => {
   
    const a = 1
    console.log(a)
    const response = await fetch('/load-employees')   
   // const data = await response.json()

}



//Actual execution path


document.getElementById('sendCompanyUrl').addEventListener('click', searchByCompanyUrl)
document.getElementById('refreshBtn').addEventListener('click', refreshTimeLine)

console.log("Starting the execution path 1")
displayFetchedData(true)

//Ihar additions on Nov 30th 2020

//dodgyfunction(true)
 //const aaa = await fetch('/load-JSON-file')  
// const data = await aaa.json()

