const express = require('express');
const router = express.Router();
const indexController = require('../controllers/IndexController')
var Tabulator = require('tabulator-tables');

/* GET home page. */
router.get('/', indexController.indexPage)

/* Fetch data for Time Lines by company name */
router.post('/fetch-timeline-data', indexController.getDataForTimeLine) //"get" 

router.post('/fetch-profile', indexController.fetchProfile)

/* Get names of employees */
router.post('/get-names', indexController.getNames)

/* Save the recently downloaded pfoile into the JSON repository */
router.post('/save-profile', indexController.saveProfile)

router.post('/launch-employees', indexController.launchEmployees)

/* Refresh ex-employees data */
router.get('/refresh-ex-employees', indexController.refreshExEmployees)

/* Refresh employees scraper data */
router.get('/refresh-employees-scraper', indexController.refreshEmployeesScraper)

/* Refresh ex-employees scraper data */
router.get('/refresh-ex-employees-scraper', indexController.refreshExEmployeesScraper)

/* Check API progress */
router.post('/check-api-progress', indexController.checkApiProgress)

/* Rewrite file for Time Lines */
router.post('/rewrite-timeline-file', indexController.rewriteTimeLineFile)

/* Search Time Lines data by company LinkedIn url */
router.post('/launch-current', indexController.launchCurrent)

router.post('/save-employee-list', indexController.saveEmployeeList)

router.post('/launch-company-info', indexController.launchCompanyInfo)

router.post('/save_company_info', indexController.saveCompanyInfo)

router.post('/fetch-company-info', indexController.fetchCompanyInfo)


router.post('/add-to-queue', indexController.addToQueue)




module.exports = router;
