
const timeLineTypes = {
    ex: 'exTimeLine',
    current: 'timeLine',
}


var dataFetched
var globalTable
var workingData
var currentCompanyName
var googleTableEmployeeType = 'current'


/**
 * Prepare fetched data and display it.
 *
 * @returns {Promise<void>}
 */
const displayFetchedData = async () => {
    
    enableButtons();

    //retrieve the current company from the last session or from the selector if starting for the first time
    var CompanyName = localStorage.getItem('CompanyName');
    if (CompanyName == null) {
            CompanyName = document.getElementById('listOfFiles').value
    }
    else{
        document.getElementById('listOfFiles').value = CompanyName;
    }
    //console.log("current company = " + CompanyName)

    //get the data from the server
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({CompanyName})
    }
    const response = await fetch('/fetch-timeline-data',options)   
    const data = await response.json()
    dataFetched = data
    workingData = data

    //do all of the context setting on the list
        //Count employees by type
        let n = 0; 
        let m = 0;
        if (data.data_1.length !== 0) {
                n = data.data_1.length
        }
        if (data.data_2.length !== 0) {
                m = data.data_2.length
        }

        let page_ex = 0
        let page_current = 1
        if (n > 0) {
            page_current = Math.round(n/10)
        }

        if (m > 0) {
            page_ex = Math.round(m/10)
        }

        localStorage.setItem('page_current',page_current)
        localStorage.setItem('page_ex',page_ex)
        console.log(page_current,page_ex)


        //get the CompanyID URL as well so that it can be accssed later

        let mainCompanyID = ""; 
        let companyUrl = "";
        var Company_Name;

        const response_1 = await fetch('/fetch-company-info',options) 
        const data_1 = await response_1.json()

        try {
             mainCompanyID = data_1[0].mainCompanyID;
             Company_Name = data_1[0].name;
        } catch(err) {
            console.log("ERROR: could not find the info file for the given company")
        }  
        localStorage.setItem('CompanyID',mainCompanyID)

        try {
             companyUrl = data_1[0].companyUrl;
        } catch(err) {
            console.log("ERROR: could not find the info file for the given company")
        }  
        localStorage.setItem('companyUrl',companyUrl)

        document.querySelector('.navigation-bar .company-name').textContent = Company_Name


        ///TABULATOR ================================================================


        assignGroups(); // assign groups to both employee lists (it saves changes to dataFetched)
        workingData = dataFetched.data_1;

        drawTabulator();






        // Grouping end  ================================================



    refreshFrequencyCounts(data.data_1)




  




    ///END TABULATOR ================================================================



    drawGoogleTimeline();


    //OLD HTML TABLE FROM RASUL -----   section that is responsible for both of the employee list tables

//     //delete the current entries in both tables
//     var tableHeaderRowCount = 1;
//     var table = document.getElementById('current-summary-table');
//     var rowCount = table.rows.length;
//     for (var i = tableHeaderRowCount; i < rowCount; i++) {
//         table.deleteRow(tableHeaderRowCount);
//     }

//     //delete the current entries in both tables
//     var tableHeaderRowCount = 1;
//     var table = document.getElementById('past-summary-table');
//     var rowCount = table.rows.length;
//     for (var i = tableHeaderRowCount; i < rowCount; i++) {
//         table.deleteRow(tableHeaderRowCount);
//     }

//     //print into the ex employees table
//     if (data.data_2 != undefined)
//     {
//         for (let i = 0; i < data.data_2.length; i++) 
//         {

//             var fetched = "available to fetch"
//             var colour = ""
//             if ( data.data_2[i].jobs != undefined) {
//                 fetched = "fetched"
//                   colour = 'class="grey"'
//             }

//             $('#past-summary-table tbody').append(
//             `<tr  ${colour}>
//                 <td>
//                     ${data.data_2[i].name}
//                 </td>
//                 <td>
//                     ${data.data_2[i].job}
//                 </td>
//                 <td>
//                     ${data.data_2[i].location}
//                 </td>
//                 <td>
//                    ${fetched}
//                 </td>
//             </tr>`)
//         }

//      //add the functionality for selecting the item in the table
//         $('#past-summary-table tr').click(function(){
//             $(this).addClass('selected')    
//             var employeeName =$(this).find('td:first').html();
//             addToQueue(employeeName);

//          });

//     }




//     //print into the current table
//     for (let i = 0; i < data.data_1.length; i++) {
        
//             var fetched = "available to fetch"
//             var colour = ""
//             if ( data.data_1[i].jobs != undefined) {
//                 fetched = "fetched"
//                 colour = 'class="grey"'
//             }

//             $('#current-summary-table tbody').append(
                
//                 `<tr  ${colour}>
//                     <td>
//                         ${data.data_1[i].name}
//                     </td>
//                     <td>
//                        ${data.data_1[i].job}
//                     </td>
//                     <td>
//                         ${data.data_1[i].location}
//                     </td>
//                     <td>
//                        ${fetched}
//                     </td>
//                 </tr>`
//             )       
//     }

//     //add the functionality for selecting the item in the table
//     $('#current-summary-table tr').click(function(){
//         $(this).addClass('selected')    
//         var employeeName =$(this).find('td:first').html();
//         addToQueue(employeeName);

//      });

    
//     $('#timeLineView-tab').click(function(){
//         //$(this).addClass('selected')    
//         //var employeeName =$(this).find('td:first').html();
//         //addToQueue(employeeName);
//         console.log("clcking")
//         //displayFetchedData(true)

//      });

}


const refreshGoogleTimeline = async function ()  {

    var CompanyName = localStorage.getItem('CompanyName');
    //donwload new data
    //get the data from the server
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({CompanyName})
    }
    const response = await fetch('/fetch-timeline-data',options)   
    const data = await response.json()
    dataFetched = data
    //workingData = data


    drawGoogleTimeline();



}



const drawGoogleTimeline = () => {


    //graph the timeline using google timeline  
    let currentEmployees = []
    let exEmployees = []
    var totalList = []
    var CompanyName = localStorage.getItem('CompanyName');


    var data = dataFetched;

    //if nwe company and still loading the employee list
    if (data.data_1.length == 0) {
        totalList[0] = [
            "list loading",
            " ",
            '#4d94ff',
            new Date("2018", "1", 1),
            new Date("2020", "1", 1)
        ]
    }
    else{

        currentEmployees = prepareDataForTimeLine(data.listOfEmployees[0],companyUrl)
        exEmployees = prepareDataForTimeLine(data.listOfExEmployees[0],companyUrl)
        totalList = currentEmployees.concat(exEmployees)
    
    }
    if ((currentEmployees.length == 0) && (exEmployees == null)) { // if none of the profiles have been downloaded

        //totalList = []
        totalList[0] = [
            "load more employees",
            "load more employees",
            '#4d94ff',
            new Date("2018", "1", 1),
            new Date("2020", "1", 1)
        ]
    }
   
    displayGoogleTimeLine(totalList, CompanyName, timeLineTypes.current)
 
    

}





const assignGroups = () => {


    assignGroupToList(dataFetched.data_1);
    assignGroupToList(dataFetched.data_2)

}


function assignGroupToList(employee_list) {

    var x = 0
    var array1 = ["Founder","founder"]
    var array2 = ["Chief", "chief", "CEO", "CFO", "CTO","COO","CMO"]
    var array3 = ["Head","head"]
    var array4 = ["Director","VP","President","president"];

    for (x=0; x < employee_list.length; x++) {

        var title1 = employee_list[x].job;
        employee_list[x].OrganicOrder = x; 
        
        if (containsTitle(title1,array1) == true)
        {
            employee_list[x].Grouping = "Founder" 
            employee_list[x].GroupOrder = 1 
        }
        else if (containsTitle(title1,array2) == true) {
            employee_list[x].Grouping = "CXO" 
            employee_list[x].GroupOrder = 2 
        }
        else if (containsTitle(title1,array3) == true) {
            employee_list[x].Grouping = "Head of ..." 
            employee_list[x].GroupOrder = 3 
        }
        else if (containsTitle(title1,array4) == true) {
            employee_list[x].Grouping = "Senior Managment" 
            employee_list[x].GroupOrder = 4 
        }
        else {
            employee_list[x].Grouping = "Other" 
            employee_list[x].GroupOrder = 5 
        }

    }

    return employee_list;
}

const containsTitle = (job,title_array) => {

    var found = false;
    for (i=0; i<title_array.length; i++) {

        if (job.indexOf(title_array[i]) != -1) {
                found = true;
                //break
        }
        
    }

    return found;

}

const prepareSankey = (data) => {


    let ex_employees = dadataFetchedta.data_2
    let roles = []
    var clean_company_name = ""

    
    //1) find the possible roles in this job
    var i; var j; var past_job;
    for (i = 0; i < ex_employees.length; i++) {
            if (ex_employees[i].jobs !== undefined) {
                var x = ex_employees[i].jobs.length             
                for (j = 0; j <x ; j++) {
                    if (ex_employees[i].jobs[j].companyUrl === companyUrl){ //if he works at the current company
                        
                        var UID = ex_employees[i].jobs[j].companyName + " - " + ex_employees[i].jobs[j].jobTitle
                        var a = roles.includes(UID)
                        if (a === false) {
                            roles.push(UID);    
                        }
                        clean_company_name = ex_employees[i].jobs[j].companyName
                    }
                }         
            }
    }
    //console.log(roles)

    //2) Find the sunsequent role frequency list  for each role

    var roles_list = []
    roles.forEach(async function (role) { 

   
        let frequency_list = []
        var i; var j; var next_job;
        for (i = 0; i < ex_employees.length; i++) {
                if (ex_employees[i].jobs !== undefined) {

                    var x = ex_employees[i].jobs.length
                    for (j = 0; j <x ; j++) {
                        if (ex_employees[i].jobs[j].companyUrl === companyUrl){ //matched companies

                            var UID = clean_company_name + " - " + ex_employees[i].jobs[j].jobTitle

                            if(UID === role ) { //matched roles
                                //save the next job that occured after this one
                                next_job = ex_employees[i].jobs[j-1].companyName +" - " + ex_employees[i].jobs[j-1].jobTitle;
                                //james stevens fix
                                if (next_job !== UID)
                                {
                                    frequency_list.push(next_job);
                                }
                            }
                        }
                    }         
                }
        }
    

        const map = frequency_list.reduce((acc, e) => acc.set(e, (acc.get(e) || 0) + 1), new Map());
        var z = []
        for (const item of map) {
            z.push(item)
            //console.log(z);
        }

       roles_list.push({'role': role, 'frequency_list': z}) // save it down

      }) //end of for each

 
        
    if (roles_list.length != 0){
            drawSankey(roles_list)

    }


}


const refreshFrequencyCounts = (data) => {

    if (data.length !== 0) 
    {

        var counts = [];
        for (i=0; i<data.length; i++)
        {
            var title = data[i].job
            try {
                var clean_title = title.split(" at ")[0]
                title = clean_title
            }
            catch{
                //do nothing
            }
        
            if (counts.length == 0) {
                counts.push([title, 1])
                continue
            }

            var found_match = false
            for (j=0; j<counts.length; j++)
            {
                if(counts[j][0] == title) {
                    counts[j][1] =  counts[j][1] + 1
                    found_match = true
                    break
                }
            }

            if (found_match == false) {
                counts.push([title, 1])
            }
        }

        //take top 3
        var first ; var second; var third; 

        first = popMax(counts);
        second = popMax(first.data)
        third = popMax(second.data)
        
        //update the HTML
        maxCharLength = 20

        var str = (first.title.length < maxCharLength) ? first.title : first.title.slice(0,maxCharLength) + "..."
        document.getElementById('jobSelectorLabel1').textContent = str + " ("+ first.frequency +")";
        document.getElementById('jobSelector1').value = first.title

        var str = (second.title.length < maxCharLength) ? second.title : second.title.slice(0,maxCharLength) + "..."
        document.getElementById('jobSelectorLabel2').textContent = str + " ("+ second.frequency +")";
        document.getElementById('jobSelector2').value = second.title

    // document.getElementById('jobSelectorLabel2').textContent = second.title + " ("+ second.frequency +")";
    //document.getElementById('jobSelectorLabel3').textContent =  third.title + " ("+ third.frequency +")";

        var str = (third.title.length < maxCharLength) ? third.title : third.title.slice(0,maxCharLength) + "..."
        document.getElementById('jobSelectorLabel3').textContent = str + " ("+ third.frequency +")";
        document.getElementById('jobSelector3').value = third.title

    }
    else { //if data is empty

        var str = " "
        document.getElementById('jobSelectorLabel1').textContent = str 
        document.getElementById('jobSelectorLabel2').textContent = str
        document.getElementById('jobSelectorLabel3').textContent = str



    }

}




const popMax = (data) => {

    var max_index = 1
    var index = 0
    var max = 0

    for (i=0; i<data.length; i++){

        if  (data[i][1] > max ) {
            title = data[i][0]
            max = data[i][1];
            max_index = i;
        }       
    }
    var data_new = []
    var j = 0
    for (i=0; i<data.length; i++){

        if (i != max_index ) {
            data_new[j] = data[i]
            j = j+1
        }
        else {}
    }

    var result = {'title': title, 'frequency': max, 'data': data_new}
    return result;

}



const drawTabulator =  (groupedCXO) => {


    var tabledata = prepareTabulatorData();

    // var employees = workingData
    // var tabledata = [];
    
    // if (employees.length == 0) { // if array is empty

    //     data.addRows(    [
    //         [" ",
    //         " ",
    //         " ", 
    //         " "]
    //       ])  

    // }
    // else {

    //     for (i=0;i<employees.length; i++) {

    //         var status;
    //         if (employees[i].jobs == undefined) {
    //             status = "available to fetch"
    //         }
    //         else {
                
    //             var string = employees[i].jobs
    //             if (string.indexOf("loading") != -1)
    //             {
    //                 status = employees[i].jobs   
    //             }
    //             else {
    //                 status = "fetched"    

    //             }
    //         }


    //         //catch blank employee names
    //         if (employees[i].name == null){
    //             var name = "[outside of your network]"
    //         }
    //         else{
    //             var name = employees[i].name
    //         }

    //         tabledata.push(
    //             {
    //             'id': i,
    //             'Name': name,
    //             'Title': employees[i].job,
    //             'Location': employees[i].location,
    //             'Status': status,
    //             'Grouping': employees[i].Grouping,
    //             }
    //             );

    //     }
    // }


    //create Tabulator on DOM element with id "example-table"
    var table = new Tabulator("#tabulator_div", {
        //height:205, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
        layout:"fitColumns", //fit columns to width of table (optional)
        columns:[ //Define Table Columns
            {title:"#", field:"id", width:50},
            {title:"Name", field:"Name", align:"left",width:300},
            {title:"Title", field:"Title" },
            {title:"Location", field:"Location",width:250},
            {title:"Status", field:"Status",width:150}
        ],


        rowClick:function(e, row){ // ==================== FetchProfile logic upon click

            //retrieve the info about the loading employee
            var index; 
            var employeeName;
            for (i=0;i<workingData.length;i++) {
                    if (workingData[i].name == row.getData().Name) {
                        employeeName = row.getData().Name;
                        index = i;
                        workingData[i].jobs = "loading"
                        tabledata = prepareTabulatorData();
                        globalTable.updateData(tabledata);
                        break
                    }
            }

            addToQueue(employeeName) // fetch profile

            var seconds = 30;
            var id = setInterval(counter, 1000);
            function counter() {
                if (seconds < 1) {

                    for (i=0;i<workingData.length;i++) {
                        if (workingData[i].name == employeeName) {
                            workingData[i].jobs = "fetched";
                            tabledata = prepareTabulatorData();
                            globalTable.updateData(tabledata);
                            clearInterval(id);
                        }
                    }
                } else {
                  seconds--; 
                  for (i=0;i<workingData.length;i++) {
                        if (workingData[i].name == employeeName) {
                            workingData[i].jobs = "loading: " + seconds + "s left "
                            tabledata = prepareTabulatorData();
                            globalTable.updateData(tabledata);
                        }
                  }

                }
             }

        },
        

        rowFormatter:function(row){
            var data = row.getData();
            if(row.getData().Status == "fetched"){
                row.getElement().style.backgroundColor = "#EDEDED"; //apply css change to row element
                row.getElement().style.color = "#aeaeaf";
            }

            if(row.getData().Status.indexOf("loading") != -1){
                row.getElement().style.backgroundColor = "#FDF8C0 "; //apply css change to row element
            }
        },

        //string.indexOf("loading") != -1
        //groupBy:"Location",

        
    });

    if (groupedCXO == true) {
        table.setGroupBy("Grouping");
        //table.setSort("Title", "asc")
    }

    table.setData(tabledata);

    globalTable = table;



}



const prepareTabulatorData =  () => {

    var employees = workingData
    var tabledata = [];
    
    if (employees.length == 0) { // if array is empty

        data.addRows(    [
            [" ",
            " ",
            " ", 
            " "]
          ])  

    }
    else {

        for (i=0;i<employees.length; i++) {

            var status;
            if (employees[i].jobs == undefined) {
                status = "available to fetch"
            }
            else {
                
                var string = employees[i].jobs
                if (string.indexOf("loading") != -1)
                {
                    status = employees[i].jobs   
                }
                else {
                    status = "fetched"    

                }
            }


            //catch blank employee names
            if (employees[i].name == null){
                var name = "[outside of your network]"
            }
            else{
                var name = employees[i].name
            }

            tabledata.push(
                {
                'id': i,
                'Name': name,
                'Title': employees[i].job,
                'Location': employees[i].location,
                'Status': status,
                'Grouping': employees[i].Grouping,
                }
                );

        }
    }

    return tabledata;

}





//draws the interactive table
const drawGoogleTable =  () => {

   // var employees = data
    
//     if (table_id == "google_table_div_current") {
//         employees = data_0.data_1
//     }
//     else {
//         employees = data_0.data_2
//     }

     var employees = workingData


      google.charts.load('current', {'packages':['table']});
      google.charts.setOnLoadCallback(drawTable);

      function drawTable() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Title');
        data.addColumn('string', 'Location');
        data.addColumn('string', 'Status');


        if (employees.length == 0) { // if array is empty

            data.addRows(    [
                [" ",
                " ",
                " ", 
                " "]
              ])  

        }
        else {

            for (i=0;i<employees.length; i++) {

                var status;
                if (employees[i].jobs == undefined) {
                    status = "available to fetch"
                }
                else {
                    
                    var string = employees[i].jobs
                    if (string.indexOf("loading") != -1)
                    {
                        status = employees[i].jobs   
                    }
                    else {
                        status = "fetched"    

                    }
                }


                //catch blank employee names
                if (employees[i].name == null){
                    var name = "[outside of your network]"
                }
                else{
                    var name = employees[i].name
                }

                data.addRows(    [
                    [name,
                    employees[i].job,
                    employees[i].location, 
                    status]
                ]);

            }
    }

       
        //column wisth - hardcode them
        data.setProperty(0, 0, 'style', 'width:250px');
       // data.setProperty(0, 2, 'style', 'width:300px');
        data.setProperty(0, 2, 'style', 'width:280px');
        data.setProperty(0, 3, 'style', 'width:150px');


        var table = new google.visualization.Table(document.getElementById('google_table_div'));
        table.draw(data, {showRowNumber: true, width: '98%', height: '100%', allowHtml: true});

//         data.setCell(0,0,"xxxx")
//          table.draw(data, {showRowNumber: true, width: '98%', height: '100%', allowHtml: true});


        // Every time the table fires the "select" event, it should call your
        // selectHandler() function.
        google.visualization.events.addListener(table, 'select', selectHandler);
        function selectHandler(e) {

             var selection = table.getSelection();
             var item = selection[0];
             var name = data.getFormattedValue(item.row, 0);
             //var name = data.getFormattedValue(item.row, 0);


             //alert('Loading ' + name);
             addToQueue(name);


            //update the employee record to "loading"
            var index;
            for (i=0;i<workingData.length;i++) {
                    if (workingData[i].name == name) {
                        workingData[i].jobs = "loading"
                        index = i;
                        break
                    }
            }

            //set up the progress bar
            var seconds = 25;
            var id = setInterval(counter, 1000);
            function counter() {
                if (seconds == 0) {
                  workingData[index].jobs = "fetched"
                  clearInterval(id);
                } else {
                  seconds--; 
                  
                  workingData[index].jobs = "loading: " + seconds + " left "
                  drawGoogleTable();

                }
             }

            

        }


//          formatting logic

//         var formatter = new google.visualization.ColorFormat();
//         formatter.addRange(-20000, 0, 'white', 'orange');
//         formatter.addRange(20000, null, 'red', '#33ff33');
//         formatter.format(data, 1); // Apply formatter to second column

//         data.setRowProperty(2,  'font-style:bold');

   
      }

  

}





     


















const drawSankey = async (roles_list) => {


    let companyName = roles_list[0].role.split(" - ")[0]
    let companyUrl = localStorage.getItem('companyUrl')



    /**
     * ---------------------------------------
     * This demo was created using amCharts 4.
     * 
     * For more information visit:
     * https://www.amcharts.com/
     * 
     * Documentation is available at:
     * https://www.amcharts.com/docs/v4/
     * ---------------------------------------
     */

    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    var chart = am4core.create("chartdiv", am4charts.SankeyDiagram);
    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
    chart.data = []


    //create the data for the chart in their format
    for (i=0; i<roles_list.length; i++) {

        var from_role = roles_list[i].role
        var from_role = roles_list[i].role

            for (j=0; j<roles_list[i].frequency_list.length; j++) { // for each subsequnt role in frequency list
                   
                  var from_role = roles_list[i].role.split(" - ")[1]
                  var from_company = roles_list[i].role.split(" - ")[0]
                  var to_role = roles_list[i].frequency_list[j][0].split(" - ")[1]
                  var to_company = roles_list[i].frequency_list[j][0].split(" - ")[0]

                  //if going externally - then show the company only 
                  if (to_company !== companyName) 
                  {
                        to_role = to_company
                  }

                  var value = roles_list[i].frequency_list[j][1]

                  //color from nodes differently for the company
                  if (from_company == companyName  )
                  {
                       var color =  "#d60606"    // bright red  
                       chart.data.push({ from: from_role,"nodeColor":color,id:"A2-0" })
                       //chart.data.push({ from: from_role,"nodeColor":color,id:"A2-0" })
                  }
                  //color from nodes differently for the company
                  if (to_company == companyName  )
                  {
                       var color =  "#d60606"    // bright red  
                       chart.data.push({ from: to_role,"nodeColor":color,id:"A2-0" })
                       //chart.data.push({ from: from_role,"nodeColor":color,id:"A2-0" })
                  }

                   
                  //append to the list
                  chart.data.push({ from: from_role, to: to_role, value: value,id:"A2-0" })
                  
            }
    }
    
    var a = 1
    //chart.sortBy = "value"


//    chart.data = [
//       { from: "AC", to: "left", value: 2, id:"A0-0", "nodeColor":"#d60606"},
//       { from: "SAC", to: "Cons", value: 3, id:"A0-0", "nodeColor":"#d60606" },
//       { from: "Cons", to: "Manager", value: 1, id:"A1", "nodeColor":"#d60606" },
//       { from: "Manager", to: "Principal", value: 1, id:"A2-0" },

//       { from: "Principal", to: "Partner", value: 1, id:"A2-0" },

//       { from: "Cons", to: "Something random", value: 1, id:"A1" },
//       { from: "AC", to: "SAC", value: 8, id:"A0-0" }

//       ]


/*
    chart.data = [
      { from: "A", to: "E", value: 2, id:"A0-0" },
      { from: "A", to: "F", value: 1, id:"A1-0" },
      { from: "A", to: "G", value: 1, id:"A2-0" },

      { from: "B", to: "E", value: 1, id:"B0-0" },
      { from: "B", to: "F", value: 1, id:"B1-0" },
      { from: "B", to: "G", value: 1, id:"B2-0" },  

      { from: "C", to: "F", value: 1, id:"C0-0" },
      { from: "C", to: "G", value: 1, id:"C1-0" },
      { from: "C", to: "H", value: 1, id:"C2-0" },    

      { from: "D", to: "E", value: 1, id:"D0-0" },
      { from: "D", to: "F", value: 1, id:"D1-0" },
      { from: "D", to: "G", value: 1, id:"D2-0" },      
      { from: "D", to: "H", value: 1, id:"D3-0" },        

      { from: "E", to: "I", value: 1, id:"A0-1" },
      { from: "E", to: "I", value: 1, id:"B0-1" },      
      { from: "E", to: "L", value: 1, id:"D0-1" },          

      { from: "F", to: "I", value: 1, id:"A1-1" },
      { from: "F", to: "I", value: 1, id:"C0-1" },      
      { from: "F", to: "I", value: 1, id:"D1-1" },          
      { from: "F", to: "M", value: 1, id:"B1-1" },          

      { from: "G", to: "I", value: 1, id:"A2-1" },
      { from: "G", to: "I", value: 1, id:"B2-1" },      
      { from: "G", to: "J", value: 1, id:"C1-1" },          
      { from: "G", to: "N", value: 1, id:"D2-1" },  

      { from: "H", to: "K", value: 1, id:"C2-1" },          
      { from: "H", to: "N", value: 1, id:"D3-1" },    

      { from: "I", to: "O", value: 1, id:"A0-2" },
      { from: "I", to: "O", value: 1, id:"B2-2" },      
      { from: "I", to: "Q", value: 1, id:"A1-2" },          
      { from: "I", to: "R", value: 1, id:"A2-2" },          
      { from: "I", to: "S", value: 1, id:"D1-2" },
      { from: "I", to: "T", value: 1, id:"B0-2" },          
      { from: "I", to: "Q", value: 1, id:"C0-2" },          

      { from: "J", to: "U", value: 1, id:"C1-2" },            

      { from: "K", to: "V", value: 1, id:"C2-2" },              
      { from: "M", to: "U", value: 1, id:"B1-2" },                

      { from: "N", to: "Q", value: 1, id:"D2-2" },                
      { from: "N", to: "Q", value: 1, id:"D3-2" },

      { from: "L", to: "W", value: 1, id:"D0-2" }
    ];
    */

    let hoverState = chart.links.template.states.create("hover");
    hoverState.properties.fillOpacity = 0.6;

    chart.dataFields.fromName = "from";
    chart.dataFields.toName = "to";
    chart.dataFields.value = "value";
    chart.dataFields.color = "nodeColor";

    chart.links.template.propertyFields.id = "id";
    chart.links.template.colorMode = "solid";
    chart.links.template.fill = new am4core.InterfaceColorSet().getFor("alternativeBackground");
    chart.links.template.fillOpacity = 0.1;
    chart.links.template.tooltipText = "";

    // highlight all links with the same id beginning
    chart.links.template.events.on("over", function(event){
      let link = event.target;
      let id = link.id.split("-")[0];

      chart.links.each(function(link){
        if(link.id.indexOf(id) != -1){
          link.isHover = true;
        }
      })
    })

    chart.links.template.events.on("out", function(event){  
      chart.links.each(function(link){
        link.isHover = false;
      })
    })

    // for right-most label to fit
    chart.paddingRight = 300;
    chart.paddingLeft = 300;


    // make nodes draggable
    var nodeTemplate = chart.nodes.template;
    nodeTemplate.inert = true;
    nodeTemplate.readerTitle = "Drag me!";
    nodeTemplate.showSystemTooltip = true;
    nodeTemplate.width = 20;

    // make nodes draggable
    var nodeTemplate = chart.nodes.template;
    nodeTemplate.readerTitle = "Click to show/hide or drag to rearrange";
    nodeTemplate.showSystemTooltip = true;
    nodeTemplate.cursorOverStyle = am4core.MouseCursorStyle.pointer
    nodeTemplate.nameLabel.label.fontSize = 12;
    nodeTemplate.nameLabel.label.wrap = true
    nodeTemplate.nameLabel.label.truncate = false
    //label.truncate = true;



}



/**
 * Append loading line to DOM.
 */
const createLoadingLine = () => {
    if (document.getElementById('timeLineRefreshing') != null) {
        document.getElementById('timeLineRefreshing').remove()
    }

    const span = document.createElement('span')
    span.innerHTML = '<span id="timeLineRefreshing">' +
        '                 Timeline data is still refreshing...' +
        '             </span>'
    document.querySelector('.refresh-block').append(span)
}

/**
 * Prepare fetched data for displaying.
 *
 * @param data
 * @returns {[]}
 */
const prepareDataForTimeLine = (data,companyUrl) => {
   
    let resultedArray = []
    if (data == undefined)
    {
        return
    }
    for (let i = 0; i < data.length; i++) {
        
       if (data[i].jobTitle !== "[loading full employment history]")
        {

            // new colour logic
            if (data[i].companyURL == companyUrl )
            {
                var colour =  '#4d94ff' //boring grey

            }
            else {
                var colour = '#e6f0ff' //dark blue
            }

            resultedArray.push([
                data[i].name,
                data[i].jobTitle,
                colour,
                new Date(data[i].startYear, data[i].startMonth, 1),
                new Date(data[i].finishYear, data[i].finishMonth, 1)
            ])
        }
    }
    return resultedArray
}

/**
 * Display TimeLine.
//  *
//  * @param resultedArray
//  */
const displayGoogleTimeLine = (resultedArray, companyName, timeLineType) => {
    google.charts.load('current', {'packages': ['timeline']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        let elementType = {}

        if (timeLineType === 'exTimeLine') {
            elementType = {elementId: timeLineTypes.ex, description: 'Ex-employees'}
        } else {
            elementType = {elementId: timeLineTypes.current, description: 'Current employees'}
        }

        var container = document.getElementById(elementType.elementId);
        var chart = new google.visualization.Timeline(container);
        var dataTable = new google.visualization.DataTable();


        dataTable.addColumn({type: 'string', id: 'Name'});
        dataTable.addColumn({type: 'string', id: 'Job title'});
        dataTable.addColumn({ type: 'string', id: 'style', role: 'style' });
        dataTable.addColumn({type: 'date', id: 'Start'});
        dataTable.addColumn({type: 'date', id: 'End'});
        dataTable.addRows(resultedArray)


        //Adjust x-axis
        let TimeLineYears = localStorage.getItem('TimeLineYears')
        
        if (TimeLineYears === "0")
        {
                var options = {
                avoidOverlappingGridLines: false,
                hAxis: {
                    minValue: new Date(2000, 0, 0),
                    maxValue: new Date(2020, 0, 0)
                }
            }
        }

        if (TimeLineYears === "10")
        {
                var options = {
                avoidOverlappingGridLines: false,
                hAxis: {
                    minValue: new Date(2010, 0, 0),
                    maxValue: new Date(2020, 0, 0)
                }
            }
        }
        if (TimeLineYears === "3")
        {
                var options = {
                avoidOverlappingGridLines: false,
                hAxis: {
                    minValue: new Date(2017, 0, 0),
                    maxValue: new Date(2020, 0, 0)
                }
            }
        }

        chart.draw(dataTable, options)
        prependCompanyNameTitle(container, `${elementType.description}`)
//                prependCompanyNameTitle(container, `${companyName} - ${elementType.description}`)

        document.querySelector('.lds-dual-ring').style.display = 'none'

    }
}





/**
 * Prepend company name into DOM.
 *
 * @param companyName
 */
const prependCompanyNameTitle = (container, companyName) => {
    const companyNameNode = document.createElement('h2')
    companyNameNode.innerText = companyName
    companyNameNode.style.marginTop = '0'
    container.prepend(companyNameNode)
}

/**
 *
 * @returns {Promise<void>}
 */
const fetchProfile = async (employee_type,employeeName) => {
   
   // disableButtons()
   // createLoadingLine()

    let FileName = localStorage.getItem('CompanyName')
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({FileName,employee_type,employeeName})
    }


    const employeesResponse = await fetch('/fetch-profile',options)
    const employeesData = await employeesResponse.json()
    if (employeesData.status === 'success') {
     
            //if launches sucesfully kick off timer
            const containerId = employeesData.data.containerId
            const checkInterval = setInterval(async () => {
                    
                    console.log("checking with server....")
                    const apiProgress = await checkApiProgress(containerId)

                    if (apiProgress.endType === "finished" && apiProgress.exitCode === 0) { // finished already
                       

                        //refresh the page to reflect the new item
                        console.log("i have finally finished loading " + employeeName)
                        //displayFetchedData()


                        clearInterval(checkInterval)
                        let FileName = localStorage.getItem('CompanyName')                      

                       //save that profile
                        let options = {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify({containerId,FileName,employee_type})
                        }
                        var res = await fetch('/save-profile', options)
                        
                        //refresh the page to reflect the new item
                        var res1 = await res.json()
                        if (res1 == "success"){
                            refreshGoogleTimeline();
                        }
                        
       

                    }
                    if (apiProgress.exitCode === 1) { //failed
                        console.log("failed")
                        clearInterval(checkInterval)
                        createErrorLine()
                        enableButtons()
                        document.getElementById('timeLineRefreshing').remove()
                    }

                }, 3000)

    } 

    //displayFetchedData()
    enableButtons()

}

/**
 * Check status of API progress.
 *
 * @param containerId
 * @returns {Promise<any>}
 */
const checkApiProgress = async containerId => {
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({containerId})
    }
    const apiProgressResponse = await fetch('/check-api-progress', options)
    const apiProgress = await apiProgressResponse.json()

    return apiProgress
}


/**
 * Enable main buttons on page.
 */
const enableButtons = () => {
    //document.getElementById('refreshBtn').disabled = false
    document.getElementById('sendCompanyUrl').disabled = false
}

/**
 * Disable main buttons on page.
 */
const disableButtons = () => {
    document.getElementById('refreshBtn').disabled = true
    document.getElementById('sendCompanyUrl').disabled = true
}

/**
 * Append error line to DOM.
 */
const createErrorLine = (text = 'Incorrect LinkedIn url') => {
    if (document.getElementById('timeLineError') != null) {
        document.getElementById('timeLineError').remove()
    }

    const span = document.createElement('span')
    span.innerHTML = '<span id="timeLineError" style="color: red">' +
        '                 Incorrect LinkedIn url.' +
        '             </span>'
    document.querySelector('.refresh-block').append(span)
}




/**
 * Check company info API progress and fetch next API
 *
 * @param containerId
 */
const checkCurrent = (containerId, employee_type) => {
    const checkInterval = setInterval(async () => {
        console.log("waiting for Phantom Buster")
        const apiProgress = await checkApiProgress(containerId)

        if (apiProgress.endType === "finished" && apiProgress.exitCode === 0) {
            clearInterval(checkInterval)

            const options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({containerId,employee_type})
            }
            
            //save the results down

            var companyName = localStorage.getItem('CompanyName')
            var res = await fetch('/save-employee-list', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({containerId,companyName,employee_type})
            })

            var res1 = await res.json()
            if (res1 == "success"){
                displayFetchedData();
          }

        }
        if (apiProgress.exitCode === 1) {
            clearInterval(checkInterval)
            createErrorLine()
            enableButtons()
            document.getElementById('timeLineRefreshing').remove()
        }

    }, 5000)

}


const checkCompanyInfo = containerId => {

    const checkInterval = setInterval(async () => {
        const apiProgress = await checkApiProgress(containerId)

        if (apiProgress.endType === "finished" && apiProgress.exitCode === 0) {

            clearInterval(checkInterval)

            const options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({containerId})
            }
            
            const companyUrl = localStorage.getItem('companyUrl')
            const companyName = localStorage.getItem('CompanyName')

            const b = await fetch('/save_company_info', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({containerId,companyName})
            })

            
            //displayFetchedData(); //refresh the page counts and global variables on CompanyID
            //immediately kick off the other agents to deliver results in parallel
            

                const data_1 = await b.json()
                var mainCompanyID;
                try {
                    mainCompanyID = data_1[0].mainCompanyID;
                } catch(err) {
                    console.log("ERROR: could not find the info file for the given company")
                }  
                localStorage.setItem('CompanyID',mainCompanyID)

            console.log("kicking off the second list with Company ID at " + mainCompanyID)
                

            getMoreEmployees("current",2)

            setTimeout(() => { 
              getMoreEmployees("current",3)
             }, 7000);

             setTimeout(() => { 
                 getMoreEmployees("ex",1)
              }, 12000);

              setTimeout(() => { 
               getMoreEmployees("ex",2)
             }, 17000);


      
            


        }
        if (apiProgress.exitCode === 1) {
            clearInterval(checkInterval)
            createErrorLine()
            enableButtons()
            document.getElementById('timeLineRefreshing').remove()
        }

    }, 5000)
}







/**
 * Search TimeLine data by LinkedIn URL.
 *
 * @returns {Promise<void>}
 */
const searchByCompanyUrl = async () => {

    // get the variables
    const companyUrl = document.getElementById('companyUrl').value
    if (companyUrl === '') {
        createErrorLine('Empty input')
        return
    }
    localStorage.setItem('companyUrl', companyUrl);

    //isolate the company name from URL
    var a = companyUrl.split("/company/")[1]
    var companyName = a.split("/")[0]
    localStorage.setItem('CompanyName', companyName);
    currentCompanyName = companyName


    //set the blank screen for a new company
    var dropListDown = document.getElementById('listOfFiles')

    var option = document.createElement('option');
    option.text = companyName
    option.value = companyName;
    dropListDown.add(option, 0);
    dropListDown.selection = option

    displayFetchedData();


    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },  
        body: JSON.stringify({companyUrl})
    }

    //kick off the Employee list agent ===============
    const response = await fetch('/launch-current', options)
    const data = await response.json()

    if (data.status === 'success') {
        checkCurrent(data.data.containerId,"current")
    }

    //kick off the Company info agent ===============
    const response_1 = await fetch('/launch-company-info', options)
    const data_1 = await response_1.json()

    if (data_1.status === 'success') {
        checkCompanyInfo(data_1.data.containerId)
    }
    


}



const launchCompanyInfo = async () => {
    // get the variables
    const companyUrl = document.getElementById('companyUrl').value
    if (companyUrl === '') {
        createErrorLine('Empty input')
        return
    }
    localStorage.setItem('companyUrl', companyUrl);

    //kick off the Company Info agent
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({companyUrl})
    }
    const response = await fetch('/launch-company-info', options)
    const data = await response.json()

    if (data.status === 'success') {




    }
    await fetchProfile()



}






// const resizeTimeLine3 = async e => {
//     localStorage.setItem('TimeLineYears', 3);
//     displayFetchedData(true)
// }

// const resizeTimeLine10 = async e => {
//     localStorage.setItem('TimeLineYears', 10);
//     displayFetchedData(true)
// }

// const resizeTimeLineAuto = async e => {
//     localStorage.setItem('TimeLineYears', 0);
//     displayFetchedData(true)
// }

const changeCompany = async e => {
    
    let a = document.getElementById('listOfFiles').value
    localStorage.setItem('CompanyName', a);

    //reset the views to standard
    document.getElementById('tableViewOrganic').checked = true;
    document.getElementById('getCurentBtn').checked = true;
    document.getElementById('jobSelector1').checked = true;

    displayFetchedData()

}


const getMoreEmployees = async (employee_type,page) => {
    
    console.log("getting more profiles")

    //get the current URL
    var companyUrl = localStorage.getItem("companyUrl")
    var companyID = localStorage.getItem("CompanyID")

    if (companyUrl == null){ 
        alert("Warning no company URL - ABORTING")
    }

    var old_page_number = localStorage.getItem("page_ex")
    if (employee_type == "current") {
          old_page_number = localStorage.getItem("page_current")
    }
    var page_number = parseInt(old_page_number) + 1

    if (page != null) {
            page_number = page

    }

    //go to node and scrape list of employees first - > add then to the chart
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({companyUrl, companyID, employee_type, page_number})
    }
    const response = await fetch('/launch-employees', options)
    const data = await response.json()
    if (data.status === 'success') {
        checkCurrent(data.data.containerId, employee_type)
    }

}



const getMore = async (event_handler) => {

    var employee_type = "ex"

    if (event_handler.currentTarget.id === "GetMoreEmployees"){
            employee_type = "current"
    }

    getMoreEmployees(employee_type)

}

const addToQueue = async (name) => {

    if (name == undefined) {
        return
    }
    var employeeName = name.trim();  

    if (employeeName == "") { 
        return
     }
     if (employeeName == null) { 
        return
     }
    
    var companyName = localStorage.getItem('CompanyName')

    

    fetchProfile("current",employeeName)



}









const pressGetCurrentBtn = () => {


    var checkAllButton = document.getElementById("jobSelector4");     
    checkAllButton.checked = true;
    document.getElementById("tableViewOrganic").checked = true;

    //var checkbox = document.getElementById("getPastBtn");     
    //checkbox.checked = false;    

    workingData = dataFetched.data_1
    drawTabulator()
    refreshFrequencyCounts(dataFetched.data_1)

    googleTableEmployeeType = 'current'


}


const pressGetPastBtn = () => {

    var checkAllButton = document.getElementById("jobSelector4");     
    checkAllButton.checked = true;
    document.getElementById("tableViewOrganic").checked = true;

    workingData = dataFetched.data_2
    //drawGoogleTable() 
    drawTabulator();
    refreshFrequencyCounts(dataFetched.data_2)

    googleTableEmployeeType = 'past'
}



const pressJobSelector = (e) => {

    //var checkbox = document.getElementById("jobSelector4");     
    //checkbox.checked = false;

    var title = e.currentTarget.value

    if (title == "xxx") {
        title = ""
    }

    if (    googleTableEmployeeType == 'current'  ){
        workingData = selectSubset(dataFetched.data_1,title)    
    }
    else {
        workingData = selectSubset(dataFetched.data_2,title)
    }
    drawTabulator();

}


const selectSubset = (data,title) => {

    var subset_data = [];
    var j = 0;
    for (i=0; i<data.length; i++) {
        if (data[i].job.includes(title) == true) {
            subset_data[j] = data[i]
            j = j + 1;
        }
    }

    return subset_data

}

const sortByGroup = () => {

    var groupCount = 1;
    var i = 0; 
    var j = 0;
    var x = 1;
    var sortedArray = [];
    for (groupCount = 1; groupCount < 10; groupCount++) {
        for (i=0; i<workingData.length; i++) {

                if (groupCount == workingData[i].GroupOrder) {
                    sortedArray[j] = workingData[i];
                    j++;
                }
        }
    }

    sortedArray;
    workingData = sortedArray;
}


const sortByOrganic = () => {

    var count = 0;
    var i = 0; 
    var j = 0;
    var sortedArray = [];

    for (count=0; count<workingData.length; count++) {
        for (i=0; i<workingData.length; i++) {
                if (count == workingData[i].OrganicOrder) {
                    sortedArray[count] = workingData[i];
                }
        }
    }

    sortedArray;
    workingData = sortedArray;
}


const pressViewGrouped = (e) => {

    sortByGroup();
    drawTabulator(true);
}

const pressViewOrganic = (e) => {

    sortByOrganic();
    drawTabulator(false);
}





    //=========================   Actual execution path    ======================

    document.getElementById('sendCompanyUrl').addEventListener('click', searchByCompanyUrl)

    document.getElementById('listOfFiles').addEventListener('change', changeCompany)
    document.getElementById('GetMoreExEmployees').addEventListener('click', getMore)
    document.getElementById('GetMoreEmployees').addEventListener('click', getMore)

    document.getElementById('getCurentBtn').addEventListener('click', pressGetCurrentBtn)
    document.getElementById('getPastBtn').addEventListener('click', pressGetPastBtn)


    document.getElementById('jobSelector1').addEventListener('click', pressJobSelector)
    document.getElementById('jobSelector2').addEventListener('click', pressJobSelector)
    document.getElementById('jobSelector3').addEventListener('click', pressJobSelector)
    document.getElementById('jobSelector4').addEventListener('click', pressJobSelector)
    
    // grouping logic
    document.getElementById('tableViewOrganic').addEventListener('click', pressViewOrganic)
    document.getElementById('tableViewGrouped').addEventListener('click', pressViewGrouped)





    displayFetchedData()

    //const response = fetch('/test-function')



