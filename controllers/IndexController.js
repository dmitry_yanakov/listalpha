const fs = require('fs')
const path = require('path')
const fetch = require('node-fetch');
const googleHelper = require('../helpers/GoogleSheetsHelper')
const axios = require("axios")

var Tabulator = require('tabulator-tables');


var fetchQueue = []
var containerName = ""


//const scraperAgentId = '2959795597355741' //his
const scraperAgentId = '7712729483671177' //Ihars

//const exScraperAgentId = '6178664966727470' //his
const exScraperAgentId = '2244894361463191' //Ihars

// const employeesAgentId = '96326744496551' //his
const employeesAgentId = '5148634705076590' //Ihars

// const exEmployeesAgentId = '3778951883187203' //his
const exEmployeesAgentId = '5009387061944326' //Ihars

const searchExportAgentId = '5009387061944326' //Ihars

//const companyInfoAgentId = '4603055996429724' //his
const companyInfoAgentId = '1357877214204328' //Ihars

//const apiKey = 'gNCY99Qu9yqTCjjwbL23BtZfMBf6DIqamlCuga9Xz7o' //His
const apiKey = 'PWqXOYYZXMebuZn5Q0ff8e6aE5S7v7wrbNcZuGio3qE' //Ihars



/**
 * Render index page.
 *
 * @param req
 * @param res
 */
exports.indexPage = async function (req, res) {

    //loads the list
    const directoryPath = path.join(__dirname, '../company_library')
    let listOfFiles = []

    listOfFiles = fs.readdirSync(directoryPath)
    listOfFiles = [...new Set(listOfFiles)]

    res.render('index', {title: 'ProfileFetch',listOfFiles})
}


exports.fetchCompanyInfo = async function (req, res) { // retreives the basic info about this company stored in the company library folder

    let data = []
    let CompanyName = req.body.CompanyName
    let file_path = path.join(__dirname, '../company_library', CompanyName)

    try {
          if (fs.existsSync(file_path)) {
                //file exists
               const rawData = fs.readFileSync(file_path)
               data = JSON.parse(rawData)
          }
    } catch(err) {
        console.log(err)
    }

    res.json(data)

}



/**
 * Parse data for JSON file for TimeLines.
 *
 * @param req
 * @param res
 */
exports.getDataForTimeLine = async function (req, res) {

    let CompanyName = req.body.CompanyName
    let data_1 = []
    let data_2 = []
    const listOfEmployees = []
    const listOfExEmployees = []

    const filename_1 = CompanyName + "_current"
    const filename_2 = CompanyName + "_ex"
    const path_1 = path.join(__dirname, '../results_library', filename_1)
    const path_2 = path.join(__dirname, '../results_library', filename_2)
    const directoryPath = path.join(__dirname, '../results_library/')
    let listOfFiles = fs.readdirSync(directoryPath)

    var a = listOfFiles.includes(filename_1)
    if (a === true)  {
           console.log("found current employees list  - reading it")
           let rawData_1 = fs.readFileSync(path_1)
           data_1 = JSON.parse(rawData_1)
           const temp_1 = parseEmployeeList(data_1)
           listOfEmployees[0] = temp_1
    }

    var b = listOfFiles.includes(filename_2)
    if (b === true)  {
           console.log("found ex employees list  - reading it")
           let rawData_2 = fs.readFileSync(path_2)
           data_2 = JSON.parse(rawData_2)
           const temp_2 = parseEmployeeList(data_2)
           listOfExEmployees[0] = temp_2
    }
    

    res.json({listOfEmployees,listOfExEmployees,data_1,data_2})

/*


    let CompanyName = req.body.CompanyName
    const rawData_1 = fs.readFileSync(path.join(__dirname, '../results_library', CompanyName))
    const read_employees = JSON.parse(rawData_1)
    const temp_1 = parseEmployeeList(read_employees)
    
    const listOfNames = []
    listOfNames[0] = temp_1
 
    listOfEmployees = listOfNames
    res.json({listOfEmployees})

*/



}


/**
 * Parse data from list of employees.
 *
 * @param employees
 * @returns {[]}
 */
const parseDataForTimeLine = employees => {
    const monthIndex = {'Jan': 0, 'Feb': 1, 'Mar': 2, 'Apr': 3, 'May': 4, 'Jun': 5, 'Jul': 6, 'Aug': 7, 'Sep': 8, 'Oct': 9, 'Nov': 10, 'Dec': 11} //ihar
    let listOfEmployees = []
    var companyName = employees.pop().companyName

    employees.map(employee => {
        if (employee.jobs === undefined) return

        var currentEmployee = employee

        employee.jobs.forEach(job => {  //if job matches company
            if (job.companyName !== companyName) return

            let [startDate, finishDate] = job.dateRange.split(' – ')
            let [startMonth, startYear] = startDate.split(' ')

            if (startYear === undefined) {
                startYear = parseInt(startMonth)
                //startMonth = 'янв.'
                startMonth = 'Jan' //ihar
            }

            if (finishDate === 'Present') { //настоящее время
                var [finishMonth, finishYear] = [new Date().getMonth(), new Date().getFullYear()]
            } else {
                if (finishDate.split(' ').length === 1) {
                    var [finishMonth, finishYear] = [0, parseInt(finishDate)]
                } else {
                    var [finishMonth, finishYear] = finishDate.split(' ')
                    finishYear = parseInt(finishYear)
                    finishMonth = monthIndex[finishMonth]
                }
            }

            listOfEmployees.push({
                ...job,
                name: currentEmployee.general.fullName,
                startMonth: monthIndex[startMonth],
                startYear: parseInt(startYear),
                finishMonth,
                finishYear
            })

        }, employee)                      //if job matches company end
    })
    return listOfEmployees

}




/**
 * Parse data from list of employees.
 *
 * @param employees
 * @returns {[]}
 */
const parseEmployeeList = employees => {
    const monthIndex = {'Jan': 0, 'Feb': 1, 'Mar': 2, 'Apr': 3, 'May': 4, 'Jun': 5, 'Jul': 6, 'Aug': 7, 'Sep': 8, 'Oct': 9, 'Nov': 10, 'Dec': 11} //ihar
    let listOfEmployees = []
    
    var targetCompanyName = ''
    if (employees[0].jobs !== undefined)
        {targetCompanyName = employees[0].jobs[0].companyName}

    var colour = '#e6f0ff'

    employees.map(employee => {
        var currentEmployee = employee

        if (employee === null){         //dodgy fix
            return
        }
        if (employee.jobs === undefined) { //if no jobs list - go for blank entry

            listOfEmployees.push({
                companyName: "Company_name",
                companyURL: "https://www.linkedin.com/company/test/",
                dateRange: "Sep 2016 - Present",
                Desciption: "bla bla bla",
                name: currentEmployee.name + " (" + currentEmployee.job + ")", 
                startMonth: 12,
                startYear: 2010,
                finishMonth: 10,
                finishYear: 2020,
                jobTitle: "[loading full employment history]",
                location: "somewhere",
                colour: '#C8C8C8', // grey
                companyQuery: currentEmployee.query

            })
        }
        else {                     // if  jobs list exists - populate all jobs

            let job_list = employee.jobs
            job_list.map(job => { //for each job in the profile

                //parse the time data
                let [startDate, finishDate] = job.dateRange.split(' – ')
                let [startMonth, startYear] = startDate.split(' ')
                if (startYear === undefined) {
                    startYear = parseInt(startMonth)
                    startMonth = 'Jan'
                }
                if (finishDate === undefined) {finishDate = startDate } //lizzy exception
                if (finishDate === 'Present') { 
                    var [finishMonth, finishYear] = [new Date().getMonth(), new Date().getFullYear()]
                } else {
                    if (finishDate.split(' ').length === 1) {
                        var [finishMonth, finishYear] = [11, parseInt(finishDate)]
                    } else {
                        var [finishMonth, finishYear] = finishDate.split(' ')
                        finishYear = parseInt(finishYear)
                        finishMonth = monthIndex[finishMonth]
                    }
                }

                //colour logic
                colour = '#e6f0ff' //default of light blue
                if (targetCompanyName === job.companyName) 
                    { colour = '#4d94ff'} // highlight current company in dark blue

            
                listOfEmployees.push({
                    companyName: job.companyName,
                    companyURL: job.companyUrl,
                    dateRange: job.dateRange,
                    Desciption: job.companyName + " - "+ job.jobTitle,
                    name: employee.name + " (" + employee.job + ")" ,
                    startMonth: monthIndex[startMonth],
                    startYear: parseInt(startYear),
                    finishMonth: finishMonth,
                    finishYear: finishYear,
                    jobTitle: job.companyName + " - "+ job.jobTitle,
                    location: job.location,
                    colour: colour,
                    companyQuery: employee.query
                })
            })

        }
                            //if job matches company end
    })

    return listOfEmployees

}



/**
 * Get company name for TimeLine by company URL.
 *
 * @param employees
 * @param companyUrl
 *
 * @returns {string}
 */
const getCompanyName = (employees, companyUrl) => {
    let companyName = ''
    employees = JSON.parse(employees)

    for (let i = 0; i < employees.length; i++) {
        if (employees[i].jobs === undefined) continue
        for (let x = 0; x < employees[i].jobs.length; x++) {
            if (employees[i].jobs[x].companyUrl === companyUrl) {
                companyName = employees[i].jobs[x].companyName
                break
            }
        }
        break
    }
    return companyName
}

/**
 * Continues the scraping process for each member of the table
 *
 * @param req
 * @param res
 *
 * @returns {Promise<*>}
 */
exports.fetchProfile = async function(req, res) {
    
     // 1) Open the JSON file and find profile with no job data
    let fileName_1 = req.body.FileName
    let employee_type = req.body.employee_type
    let employeeName = req.body.employeeName

    var b = []

    const rawData_1 = fs.readFileSync(path.join(__dirname, '../results_library', fileName_1+"_" + "current"))
    
    try {
         const rawData_2 = fs.readFileSync(path.join(__dirname, '../results_library', fileName_1+"_" + "ex"))
         b = JSON.parse(rawData_2)
    } catch(err) {

    }  

    var a = JSON.parse(rawData_1)
    const employees = a.concat(b)


     // 2) Extract the URL for their LinkedIn profie
    let LinkedIn_url = ""
    for (let i = 0; i < employees.length; i++) {
        if (employees[i].name === employeeName) { //find the first profile with no jobs data
                LinkedIn_url = employees[i].url  
                //sometimes the fied is called profileUrl
                if (employees[i].url === undefined) {
                         LinkedIn_url = employees[i].profileUrl  
                }
                break
        }
        continue
    }
    if (LinkedIn_url === "") {
        console.log("Fetched profile already has the job list - aborting")
        return
    }
    googleHelper.updateCompanyName(LinkedIn_url) 
    console.log("fetching for " + LinkedIn_url)


     // 3) Kick off the profile scrapre for that one profile
      const options = {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'X-Phantombuster-Key-1': apiKey
            },
        }
        const url = `https://api.phantombuster.com/api/v1/agent/${scraperAgentId}/launch`

        const response = await fetch(url, options)
        const data = await response.json()
        res.json(data)
}


/**
 * Refresh employees data in PhantomBuster service.
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.refreshExEmployees = async function(req, res) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v1/agent/${exEmployeesAgentId}/launch`

    const response = await fetch(url, options)
    const data = await response.json()

    res.json(data)


}

exports.saveProfile = async function (req, res) {

    const agentId = req.body.containerId
    const fileName_1 = req.body.FileName
    const employee_type = req.body.employee_type


   //get the profile from Phantom Buster
    let url = `https://api.phantombuster.com/api/v2/containers/fetch-result-object?id=${agentId}`
    let options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key': apiKey
        }
    }
    const response = await fetch(url, options)
    const data = await response.json()
    let new_profile = JSON.parse(data.resultObject)
    if (new_profile === null) return

   // do this twice for each employee type   
   var fileType = ["ex","current"]
   fileType.forEach(myFunction)

   function myFunction(employee_type)
   {
       //Open the JSON file and append the profile
        const rawData_1 = fs.readFileSync(path.join(__dirname, '../results_library', fileName_1+"_"+employee_type))
        let read_employees = JSON.parse(rawData_1)
        //let profile_name = ""

        //find the right person to add jobs to
        for (let i = 0; i < read_employees.length; i++) {
            if (read_employees[i].name === new_profile[0].general["fullName"]) {
                  read_employees[i].jobs = new_profile[0].jobs
                  console.log("SAVING PROFILE: "+read_employees[i].name )
                  //profile_name = read_employees[i].name
            }
        }

        //save the file down
        fs.writeFileSync(path.join(__dirname, '../results_library', fileName_1+"_"+employee_type), JSON.stringify(read_employees), err => {
            if (err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        })

   }

    let profile_name = new_profile[0].general.fullName

    //also save the raw jason file into profiles, for later use
    fs.writeFileSync(path.join(__dirname, '../profiles_library', profile_name ), JSON.stringify(data), err => {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    })

    res.json("success");


}





/**
 * Get names of employees.
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.getNames = async function (req, res) { //actually is "savenames"

//    const url = `https://api.phantombuster.com/api/v2/containers/fetch-result-object?id=${agentId}`


    let url = `https://api.phantombuster.com/api/v2/containers/fetch-result-object?id=${req.body.containerId}`
    let options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key': apiKey
        }
    };

    const response = await fetch(url, options)
    const data = await response.json()


    //save the JSON file into the library
    const fileName = req.body.companyName
    fs.writeFileSync(path.join(__dirname, '../results_library', fileName), JSON.stringify(data), err => {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    })

    res.json(data)
}

/**
 * Launch ex-employees agent API.
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.launchEmployees = async function (req, res) {
   
    const companyUrl = req.body.companyUrl
    const companyID = req.body.companyID
    const employee_type = req.body.employee_type
    const page_number = req.body.page_number

    
    //modify the LinkedIn URL based on the type of request we have made
    var companyLink = ""
    if (employee_type === "ex") {
        companyLink = `https://www.linkedin.com/search/results/people/?facetPastCompany=%5B"${companyID}%22%5D&origin=FACETED_SEARCH&page=${page_number}`    
    }
    else {
     
       companyLink = `https://www.linkedin.com/search/results/people/?facetCurrentCompany=%5B"${companyID}%22%5D&origin=FACETED_SEARCH&page=${page_number}`    

       //companyLink = `https://www.linkedin.com/search/results/people/?facetCurrentCompany=%5B%222114%22%5D&origin=FACETED_SEARCH`
       //`https://www.linkedin.com/search/results/people/?facetCurrentCompany=%5B%222114%22%5D&origin=FACETED_SEARCH&page=1`
    }
    
    console.log(companyLink)

    //update Google spreadsheet
    googleHelper.updateCompanyId(companyLink)

    options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    url = `https://api.phantombuster.com/api/v1/agent/${searchExportAgentId}/launch`
    const response = await fetch(url, options)
    const data = await response.json()

    //data = []
    res.json(data)
}

/**
 * Check API progress.
 *
 * @param req
 * @param res
 *
 * @returns {Promise<void>}
 */
exports.checkApiProgress = async function (req, res) {
    const agentId = req.body.containerId
    const options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v2/containers/fetch?id=${agentId}`

    const response = await fetch(url, options)
    const data = await response.json()

    res.json(data)
}

/**
 * Refresh employees scraper in PhantomBuster service
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.refreshEmployeesScraper = async function (req, res) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v1/agent/${scraperAgentId}/launch`

    const response = await fetch(url, options)
    const data = await response.json()

    res.json(data)
}

/**
 * Refresh ex-employees scraper in PhantomBuster service
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.refreshExEmployeesScraper = async function(req, res) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v1/agent/${exScraperAgentId}/launch`

    const response = await fetch(url, options)
    const data = await response.json()

    res.json(data)
}

/**
 * Rewrite file for TimeLine.
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.rewriteTimeLineFile = async function (req, res) {
    const agentId = req.body.containerId
    const companyUrl = req.body.companyUrl
    const options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v2/containers/fetch-result-object?id=${agentId}`

    const response = await fetch(url, options)
    const data = await response.json()
    
    const companyName = getCompanyName(data.resultObject, companyUrl)
    let preparedData = JSON.parse(data.resultObject)
    preparedData.push({companyName})

    const fileName = req.body.exEmployees ? 'exResult.json' : 'result.json'

    fs.writeFileSync(path.join(__dirname, '../', fileName), JSON.stringify(preparedData), err => {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    })
    res.json("The file was saved!");
}





exports.saveEmployeeList = async function (req, res) {

    
    const agentId = req.body.containerId
    const companyName = req.body.companyName
    const employee_type = req.body.employee_type

    console.log("saving the results down to JSON file for " + companyName + " " + employee_type)


    const options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v2/containers/fetch-result-object?id=${agentId}`

    const response = await fetch(url, options)
    const data = await response.json()
    let preparedData = JSON.parse(data.resultObject)
    let data_to_save = preparedData
    

    const fileName = companyName + "_" + employee_type
    let filePath = path.join(__dirname, '../results_library', fileName)

    //check to make sure if file exists - then append the results to it
    const directoryPath = path.join(__dirname, '../results_library')
    let listOfFiles = fs.readdirSync(directoryPath)
    var a = listOfFiles.includes(fileName)

    if (a == true)  {
           console.log("found existing file - appending the new list to it")
           let rawData_1 = fs.readFileSync(filePath)
           data_to_save = JSON.parse(rawData_1)

           data_to_save = preparedData.concat(data_to_save) 


//            let clean_data_to_save = []

           //avoid duplicating records in case the indexing is off
//            for (i=0; i<data_to_save.length; i++) {
//                var dup = false
//                 for (j=0; j<preparedData.length; j++) { // 

//                     if (data_to_save[i].url != null){
//                         if (data_to_save[i].url == preparedData[j].url ) { //duplicate
//                             dup = true
//                         }
//                     }
//                     else {
//                          if (data_to_save[i].profileUrl == preparedData[j].url ) { //duplicate
//                             dup = true
//                         } 
//                     }
//                 }
//                 if (dup == false) {
//                     clean_data_to_save.push(data_to_save[i])    
//                 }
//            }
//            data_to_save = clean_data_to_save.concat(preparedData) 
    }
   
   // data_to_save = clean_data_to_save.concat(preparedData) 


    fs.writeFileSync(path.join(__dirname, '../results_library', fileName), JSON.stringify(data_to_save), err => {
        if (err) {
            return console.log(err);
        }
    })
  


    res.json("success");
}



exports.saveCompanyInfo = async function (req, res) {
    const agentId = req.body.containerId
    let companyName = req.body.companyName
    const options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v2/containers/fetch-result-object?id=${agentId}`

    const response = await fetch(url, options)
    const data = await response.json()    
    let preparedData = JSON.parse(data.resultObject)


    //open the respective JSON file with employees
    //const rawData_1 = fs.readFileSync(path.join(__dirname, '../results_library', companyName))
    //const read_employees = JSON.parse(rawData_1)
    //read_employees.push(preparedData)

    companyName = companyName

    //save the file down
    fs.writeFileSync(path.join(__dirname, '../company_library', companyName), JSON.stringify(preparedData), err => {
        if (err) {
            return console.log(err);
        }
        console.log("The Company_Info file was saved!");
    })
    res.json(preparedData);
}



exports.launchCompanyInfo = async function (req, res) {

    let companyUrl = req.body.companyUrl
    googleHelper.updateCompanyId(companyUrl)
   

    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey,
        },
    }
    let url = `https://api.phantombuster.com/api/v1/agent/${companyInfoAgentId}/launch`

    const companyInfoResponse = await fetch(url, options)
    const companyInfo = await companyInfoResponse.json()
    res.json(companyInfo)
    

}













/**
 * Search TimeLine data by company LinkedIn url.
 *
 * @param req
 * @param res
 *
 * @returns {Promise<void>}
 */
exports.launchCurrent = async function (req, res) {

    let companyUrl = req.body.companyUrl

    googleHelper.updateCompanyId(companyUrl)
    //googleHelper.updateCompanyName(req.body.companyUrl)
   
  
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey,
        },
    }
    let url = `https://api.phantombuster.com/api/v1/agent/${employeesAgentId}/launch`

    const companyInfoResponse = await fetch(url, options)
    const companyInfo = await companyInfoResponse.json()
    res.json(companyInfo)
    

}



exports.addToQueue = async function (req, res) {

    let companyName = req.body.companyName
    let employeeName = req.body.employeeName

    if (employeeName == "") { 
        return
     }
     if (employeeName == null) { 
        return
     }

    console.log("adding to Fetch Queue: " + employeeName)
    fetchQueue.push(employeeName)


    if (fetchQueue.length > 0) {


        console.log("Queue status: " + fetchQueue)

        //launch the scrapers

        //assume this is the first time - launch the scraper
        var data = await fetchProfile_2(employeeName, 'current', companyName);
        //var apiResponse_2 = await data.containerId
        const containerId = data.data.containerId
  
            console.log("kicked off PB, waiting now")


                let FileName = companyName                    

                const checkInterval = setInterval(async () => {

                    console.log("checking with Phantom Buster")

                    var apiProgress = await checkProfileScraper(containerId)
                    console.log("containter id:" + containerId)
                    //console.log ("containerName: " + containerName)

                    if (apiProgress.endType === "finished" && apiProgress.exitCode === 0) { // finished already

                            clearInterval(checkInterval)
                        


                               //get the profile from Phantom Buster
                                let url = `https://api.phantombuster.com/api/v2/containers/fetch-result-object?id=${containerName}`
                                let options = {
                                    method: 'GET',
                                    headers: {
                                        Accept: 'application/json',
                                        'X-Phantombuster-Key': apiKey
                                    }
                                }
                                const response = await fetch(url, options)
                                const data = await response.json()
                                let new_profile = JSON.parse(data.resultObject)
                                console.log(new_profile)
                                  //save that profile
                            console.log("SUCCESS: retrieved profile - saving it now")
                            
                         

                    }

                    if (apiProgress.exitCode === 1) { //failed
                        console.log("profile scraper failed")
                        clearInterval(checkInterval)
                    }

                }, 5000)

        
         //finished the PB Agent

        fetchQueue.pop(employeeName)
        //continue
    }

}



const checkProfileScraper = async function(containerId) {

    const agentId = containerId
    const options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v2/containers/fetch?id=${agentId}`

    const response = await fetch(url, options)
    //const data = await response.json()
//    res.json(data)
    return response
}


fetchProfile_2 = async function(employeeName, employee_type, fileName_1) {
    
     // 1) Open the JSON file and find profile with no job data
    //let fileName_1 = req.body.FileName
    //let employee_type = req.body.employee_type

    const rawData_1 = fs.readFileSync(path.join(__dirname, '../results_library', fileName_1+"_" + employee_type))
    const employees = JSON.parse(rawData_1)

     // 2) Extract the URL for their LinkedIn profie
    let LinkedIn_url = ""
    for (let i = 0; i < employees.length; i++) {
        if ((employees[i].jobs === undefined) && (employees[i].name === employeeName) ) { //find the right person and make sure he needs scraping
                LinkedIn_url = employees[i].url  
                //sometimes the fied is called profileUrl
                if (employees[i].url === undefined) {
                         LinkedIn_url = employees[i].profileUrl  
                }
                break
        }
        continue
    }

    googleHelper.updateCompanyName(LinkedIn_url) 


     // 3) Kick off the profile scrapre for that one profile
      const options = {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'X-Phantombuster-Key-1': apiKey
            },
        }
        const url = `https://api.phantombuster.com/api/v1/agent/${scraperAgentId}/launch`

        const response = await fetch(url, options) //LAUNCH THE AGENT
        const data = await response.json()
        
        return data
        

        //req.json(data);

//         var a = data.data.containerId

//         console.log("container ID secured: " + a)
//         containerName = a

//         return a
      

}



       // res.json(data)
