const fs = require('fs')
const path = require('path')
const fetch = require('node-fetch');
const googleHelper = require('../helpers/GoogleSheetsHelper')

//const scraperAgentId = '2959795597355741' //his
const scraperAgentId = '7712729483671177' //Ihars

//const exScraperAgentId = '6178664966727470' //his
const exScraperAgentId = '2244894361463191' //Ihars

// const employeesAgentId = '96326744496551' //his
const employeesAgentId = '5148634705076590' //Ihars

// const exEmployeesAgentId = '3778951883187203' //his
const exEmployeesAgentId = '5009387061944326' //Ihars

//const companyInfoAgentId = '4603055996429724' //his
const companyInfoAgentId = '1357877214204328' //Ihars

//const apiKey = 'gNCY99Qu9yqTCjjwbL23BtZfMBf6DIqamlCuga9Xz7o' //His
const apiKey = 'PWqXOYYZXMebuZn5Q0ff8e6aE5S7v7wrbNcZuGio3qE' //Ihars


/**
 * Render index page.
 *
 * @param req
 * @param res
 */
exports.indexPage = async function (req, res) {
    res.render('index', {title: 'Exr'})
}

/**
 * Parse data for JSON file for TimeLines.
 *
 * @param req
 * @param res
 */
exports.getDataForTimeLine = async function (req, res) {
    const fileNames = ['result.json', 'exResult.json']

    const typesOfEmployees = fileNames.map(fileName => {
        const rawData = fs.readFileSync(path.join(__dirname, '../', fileName))
        if (Object.keys(rawData).length !== 0) {
            return JSON.parse(rawData)
        }
    })

    let listOfEmployees = typesOfEmployees.map(typeOfEmployee => {
        if (typeOfEmployee !== undefined) {
            return parseDataForTimeLine(typeOfEmployee)
        }
    })
    listOfEmployees = listOfEmployees.filter(employees => employees !== undefined)

    res.json({listOfEmployees})
}

/**
 * Parse data from list of employees.
 *
 * @param employees
 * @returns {[]}
 */
const parseDataForTimeLine = employees => {
     const monthIndex = {'Jan': 0, 'Feb': 1, 'Mar': 2, 'Apr': 3, 'May': 4, 'Jun': 5, 'Jul': 6, 'Aug': 7, 'Sep': 8, 'Oct': 9, 'Nov': 10, 'Dec': 11} //ihar
    //const monthIndex = {'янв.': 0, 'февр.': 1, 'март': 2, 'апр.': 3, 'май': 4, 'июнь': 5, 'июль': 6, 'авг.': 7, 'сент.': 8, 'окт.': 9, 'нояб.': 10, 'дек.': 11 }
    let listOfEmployees = []
    var companyName = employees.pop().companyName

    employees.map(employee => {
        if (employee.jobs === undefined) return

        var currentEmployee = employee

        employee.jobs.forEach(job => {  //if job matches company
            if (job.companyName !== companyName) return

            let [startDate, finishDate] = job.dateRange.split(' – ')
            let [startMonth, startYear] = startDate.split(' ')

            if (startYear === undefined) {
                startYear = parseInt(startMonth)
                //startMonth = 'янв.'
                startMonth = 'Jan' //ihar
            }

            if (finishDate === 'Present') { //настоящее время
                var [finishMonth, finishYear] = [new Date().getMonth(), new Date().getFullYear()]
            } else {
                if (finishDate.split(' ').length === 1) {
                    var [finishMonth, finishYear] = [0, parseInt(finishDate)]
                } else {
                    var [finishMonth, finishYear] = finishDate.split(' ')
                    finishYear = parseInt(finishYear)
                    finishMonth = monthIndex[finishMonth]
                }
            }

            listOfEmployees.push({
                ...job,
                name: currentEmployee.general.fullName,
                startMonth: monthIndex[startMonth],
                startYear: parseInt(startYear),
                finishMonth,
                finishYear
            })

        }, employee)                      //if job matches company end
    })
    return listOfEmployees

}

/**
 * Get company name for TimeLine by company URL.
 *
 * @param employees
 * @param companyUrl
 *
 * @returns {string}
 */
const getCompanyName = (employees, companyUrl) => {
    let companyName = ''
    employees = JSON.parse(employees)

    for (let i = 0; i < employees.length; i++) {
        if (employees[i].jobs === undefined) continue
        for (let x = 0; x < employees[i].jobs.length; x++) {
            if (employees[i].jobs[x].companyUrl === companyUrl) {
                companyName = employees[i].jobs[x].companyName
                break
            }
        }
        break
    }
    return companyName
}

/**
 * Refresh employees data in PhantomBuster service.
 *
 * @param req
 * @param res
 *
 * @returns {Promise<*>}
 */
exports.refreshEmployees = async function(req, res) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v1/agent/${employeesAgentId}/launch`

    const response = await fetch(url, options)
    const data = await response.json()

    res.json(data)
}

/**
 * Refresh employees data in PhantomBuster service.
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.refreshExEmployees = async function(req, res) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v1/agent/${exEmployeesAgentId}/launch`

    const response = await fetch(url, options)
    const data = await response.json()

    res.json(data)
}

/**
 * Get names of employees.
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.getNames = async function (req, res) {
    let url = `https://api.phantombuster.com/api/v2/containers/fetch-result-object?id=${req.body.containerId}`
    let options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key': apiKey
        }
    };

    const response = await fetch(url, options)
    const data = await response.json()

    res.json(data)
}

/**
 * Launch ex-employees agent API.
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.launchExEmployees = async function (req, res) {
    let url = `https://api.phantombuster.com/api/v2/containers/fetch-result-object?id=${req.body.containerId}`
    let options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key': apiKey
        }
    };

    const companyInfoResponse = await fetch(url, options)
    const companyInfo = await companyInfoResponse.json()
    const companyId = JSON.parse(companyInfo.resultObject)[0]['mainCompanyID']

    googleHelper.updateCompanyId(companyId)

    options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    url = `https://api.phantombuster.com/api/v1/agent/${exEmployeesAgentId}/launch`

    const response = await fetch(url, options)
    const data = await response.json()

    res.json(data)
}

/**
 * Check API progress.
 *
 * @param req
 * @param res
 *
 * @returns {Promise<void>}
 */
exports.checkApiProgress = async function (req, res) {
    const agentId = req.body.containerId
    const options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v2/containers/fetch?id=${agentId}`

    const response = await fetch(url, options)
    const data = await response.json()

    res.json(data)
}

/**
 * Refresh employees scraper in PhantomBuster service
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.refreshEmployeesScraper = async function (req, res) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v1/agent/${scraperAgentId}/launch`

    const response = await fetch(url, options)
    const data = await response.json()

    res.json(data)
}

/**
 * Refresh ex-employees scraper in PhantomBuster service
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.refreshExEmployeesScraper = async function(req, res) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v1/agent/${exScraperAgentId}/launch`

    const response = await fetch(url, options)
    const data = await response.json()

    res.json(data)
}

/**
 * Rewrite file for TimeLine.
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.rewriteTimeLineFile = async function (req, res) {
    const agentId = req.body.containerId
    const companyUrl = req.body.companyUrl
    const options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    const url = `https://api.phantombuster.com/api/v2/containers/fetch-result-object?id=${agentId}`

    const response = await fetch(url, options)
    const data = await response.json()
    const companyName = getCompanyName(data.resultObject, companyUrl)
    let preparedData = JSON.parse(data.resultObject)
    preparedData.push({companyName})

    const fileName = req.body.exEmployees ? 'exResult.json' : 'result.json'

    fs.writeFileSync(path.join(__dirname, '../', fileName), JSON.stringify(preparedData), err => {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    })
    res.json("The file was saved!");
}

/**
 * Search TimeLine data by company LinkedIn url.
 *
 * @param req
 * @param res
 *
 * @returns {Promise<void>}
 */
exports.searchByCompanyUrl = async function (req, res) {
    googleHelper.updateCompanyName(req.body.companyUrl)
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'X-Phantombuster-Key-1': apiKey
        },
    }
    let url = `https://api.phantombuster.com/api/v1/agent/${companyInfoAgentId}/launch`

    const companyInfoResponse = await fetch(url, options)
    const companyInfo = await companyInfoResponse.json()
    res.json(companyInfo)
}
